'use strict'

const { camelCase } = require('lodash')

const Config = use('Adonis/Src/Config')
const LucidModel = use('Streams/Platform/Model/LucidModel')
const StreamModel = use('Streams/Platform/Stream/StreamModel')
const AssignmentModel = use('Streams/Platform/Assignment/AssignmentModel')

class EntryModel extends LucidModel {

  static get translationForeignKey () {
    return 'entry_id'
  }

  static get searchable () {
    return false
  }

  static get rules () {
    return {}
  }

  static get fields () {
    return {}
  }

  static get relationships () {
    return {}
  }

  static get _stream () {
    return {}
  }

  static boot () {
    // TODO
  }

  scopeSorted (builder, direction = 'asc') {
    builder.orderBy('sort_order', direction)
  }

  getId () {
    return this.getKey()
  }

  getEntryId () {
    return this.getId()
  }

  getEntryTitle () {
    return this.getTitle()
  }

  getBoundModelName () {
    // TODO
  }

  getBoundModelNamespace () {
    return this.getBoundModelName().split('/').slice(0, -1).join('/')
  }

  getSortOrder () {
    return this.sort_order
  }

  getTitle () {
    return this[this.getTitleName()]
  }

  getFieldValue (fieldSlug, locale = null) {
    let entry
    let value
    let translation
    const assignment = this.getAssignment(fieldSlug)
    const type = assignment.getFieldType()
    const accessor = type.getAccessor()
    const modifier = type.getModifier()

    if (!locale) {
      locale = Config.get('app.locale')
    }

    if (assignment.isTranslatable()) {
      entry = this.translateOrDefault(locale)
      type.setLocale(locale)
    } else {
      entry = this
    }

    type.setEntry(entry)

    value = modifier.restore(accessor.get())

    if (
      value === null &&
      assignment.isTranslatable() &&
      assignment.isRequired() &&
      (translation = this.translate())
    ) {
      type.setEntry(translation)
      value = modifier.restore(accessor.get())
    }

    return value
  }

  setFieldValue (fieldSlug, value, locale = null) {
    let entry
    const assignment = this.getAssignment(fieldSlug)
    const type = assignment.getFieldType(this)

    if (!locale) {
      locale = Config.get('app.locale')
    }

    if (assignment.isTranslatable()) {
      entry = this.translateOrNew(locale)
      type.setLocale(locale)
    } else {
      entry = this
    }

    type.setEntry(entry)

    const accessor = type.getAccessor()
    const modifier = type.getModifier()

    accessor.set(modifier.modify(value))

    return this
  }

  getField (slug) {
    const assignment = this.getAssignment(slug)

    return assignment instanceof AssignmentModel
      ? assignment.getField()
      : null
  }

  hasField (slug) {
    return this.getField(slug) !== null
  }

  getFieldType (fieldSlug) {
    let entry
    const locale = Config.get('app.locale')
    const assignment = this.getAssignment(fieldSlug)

    if (!assignment) {
      return null
    }

    const type = assignment.getFieldType()

    if (assignment.isTranslatable()) {
      entry = this.translateOrDefault(locale)
      type.setLocale(locale)
    } else {
      entry = this
    }

    type.setEntry(entry)
    type.setValue(this.getFieldValue(fieldSlug))
    type.setEntry(this)

    return type
  }

  getFieldTypeQuery (fieldSlug) {
    const type = this.getFieldType(fieldSlug)

    return type
      ? type.getQuery()
      : null
  }

  getFieldTypePresenter (fieldSlug) {
    const type = this.getFieldType(fieldSlug)

    return type
      ? type.getPresenter()
      : null
  }

  setAttribute (key, value) {
    if (
      !this.isKeyALocale(key) &&
      !this.hasSetMutator(key) &&
      this.getFieldType(key)
    ) {
      this.setFieldValue(key, value)

      return this
    }

    super.setAttribute(key, value)

    return this
  }

  getAttribute (key) {
    if (
      ['created_by', 'updated_by'].contains(key) ||
      this.relationships.contains(key)
    ) {
      return super.getAttribute(camelCase(key))
    }

    if (this.hasGetMutator(key) && this.fields.contains(key)) {
      return this.getFieldValue(key)
    }

    return super.getFieldValue(key)
  }

  getRawAttribute (key, proc = true) {
    return !proc
      ? this.getAttributeFromArray(key)
      : super.getAttribute(key)
  }

  setRawAttribute (key, value) {
    super.setAttribute(key, value)

    return this
  }

  getStream () {
    return this.stream()
  }

  getStreamNamespace () {
    return this.getStream().getNamespace()
  }

  getStreamSlug () {
    return this.getStream().getSlug()
  }

  getStreamName () {
    return this.getStream().getName()
  }

  getStreamPrefix () {
    return this.getStream().getPrefix()
  }

  getTableName () {
    return this.getStream().getEntryTableName()
  }

  getTranslationsTableName () {
    return this.getStream().getEntryTranslationsTableName()
  }

  getAssignments () {
    return this.getStream().getAssignments()
  }

  getAssignmentFieldSlugs (prefix = null) {
    return this.getAssignments().fieldSlugs(prefix)
  }

  getAssignmentsByFieldType (fieldType) {
    return this.getAssignments().findAllByFieldType(fieldType)
  }

  getAssignment (fieldSlug) {
    return this.getAssignments().getByFieldSlug(fieldSlug)
  }

  getTranslatableAssignments () {
    return this.getStream().getAssignments().translatable()
  }

  getRelationshipAssignments () {
    return this.getStream().getAssignments().relations()
  }

  getRequiredAssignments () {
    return this.getStream().getAssignments().required()
  }

  getSearchableAssignments () {
    return this.getStream().getAssignments().searchable()
  }

  isTranslatable () {
    return this.getStream().isTranslatable()
  }

  isTrashable () {
    return this.getStream().isTrashable()
  }

  lastModified () {
    return this.updated_at || this.created_at
  }

  getCreatedBy () {
    return this.created_by
  }

  createdBy () {
    return this.belongsTo(Config.get('auth.session.model'))
  }

  getUpdatedBy () {
    return this.updated_by
  }

  updatedBy () {
    return this.belongsTo(Config.get('auth.session.model'))
  }

  titleColumnIsTranslatable () {
    return this.assignmentIsTranslatable(this.getTitleName())
  }

  assignmentIsTranslatable (fieldSlug) {
    return this.isTranslatedAttribute(fieldSlug)
  }

  assignmentIsRelationship (fieldSlug) {
    return this.getRelationshipAssignments().fieldSlugs().contains(fieldSlug)
  }

  fireFieldTypeEvents (trigger, payload = {}) {
    const assignments = this.getAssignments()

    Object.keys(assignments.notTranslatable()).forEach(slug => {
      const assignment = assignments[slug]
      const fieldType = assignment.getFieldType()

      fieldType.setValue(this.getRawAttribute(assignment.getFieldSlug()))
      fieldType.setEntry(this)

      payload['entry'] = this
      payload['fieldType'] = fieldType

      fieldType.fire(trigger, payload)
    })
  }

  stream () {
    if (!(this._stream instanceof StreamModel)) {
      this._stream = new StreamModel(this._stream)
    }

    return this._stream
  }

  newCollection (items) {

  }

}

module.exports = EntryModel
