'use strict'

const { ServiceProvider } = require('@adonisjs/fold')
const { resolver } = require('@adonisjs/fold')

class StreamsServiceProvider extends ServiceProvider {

  get directories () {
    return {
      '@streams': 'Streams/Platform',
      '@streams/Model': 'Streams/Platform/Model',
    }
  }

  get schedule () {
    return {}
  }

  get listeners () {
    return {}
  }

  get bindings () {
    return {}
  }

  get singletons () {
    return {}
  }

  get providers () {
    return []
  }

  get commands () {
    return []
  }

  /**
   * Register bindings.
   *
   * @private
   *
   * @param      {Config}  Config  The configuration
   */
  _registerBindings (Config) {
    const self = this
    Object.keys(Config.merge('streams.bindings', this.bindings))
      .forEach((abstract) => {
        self.app.bind(abstract, (app) => {
          return app.make(self.bindings[abstract])
        })
      })
  }

  /**
   * Register singletons.
   *
   * @private
   *
   * @param      {Config}  Config  The configuration
   */
  _registerSingletons (Config) {
    const self = this
    Object.keys(Config.merge('streams.singletons', this.singletons))
      .forEach((abstract) => {
        self.app.bind(abstract, (app) => {
          return app.make(self.singletons[abstract])
        })
      })
  }

  /**
   * Register autoloads.
   *
   * @private
   */
  _registerAutoloads () {
    const self = this
    const { autoload } = require('@root/package.json')
    Object.keys(autoload)
      .forEach((alias) => {
        self.app.autoload(alias, autoload[alias])
      })
  }

  /**
   * Register directories.
   *
   * @private
   */
  _registerDirectories () {
    resolver.directories(this.directories)
  }

  /**
   * Register streams other providers.
   *
   * @private
   *
   * @param      {Config}  Config  The configuration
   */
  _registerProviders (Config) {
    // Config.merge('streams.providers', this.providers)
  }

  /**
   * Register commands.
   *
   * @private
   *
   * @param      {<Config>}  Config  The configuration
   */
  _registerCommands (Config) {
    Config.merge('streams.commands', this.commands)
      .forEach((command) => {
        self.app.bind(command, (app) => {
          return app.make(command)
        })
      })
  }

  register () {
    const Config = this.app.use('Adonis/Src/Config')

    this._registerProviders(Config)
    this._registerBindings(Config)
    this._registerSingletons(Config)
    this._registerAutoloads()
    this._registerDirectories()
    this._registerCommands(Config)
  }

  boot () {
    const Event = this.app.use('Adonis/Src/Event')
    // const LucidModel = this.app.use('Streams/Platform/Model/LucidModel')

    Event.fire('streams::booting')

    Event.fire('streams::booted')

    // console.log(new LucidModel({}).getPresenter())
    // console.log(this.constructor.$namespace)
    // console.log(Reflect.ownKeys(Reflect))
  }

  /**
   * Get the namespace of the class
   *
   * @return     {String}
   */
  static get $namespace () {
    const { _moduleAliases } = require('@root/package.json')
    const folder = _moduleAliases['@streams']

    return require('@adonisjs/fold').resolver
      .forDir(__dirname.replace(folder, '@streams'))
      .translate(this.name).replace('@streams/', '')
  }

}

module.exports = StreamsServiceProvider
