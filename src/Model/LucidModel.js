'use strict'

const Model = use('Adonis/Src/Model')
const LucidPresenter = use('Streams/Platform/Model/LucidPresenter')
const LucidCollection = use('Streams/Platform/Model/LucidCollection')

/**
 * Class for the base Lucid model.
 *
 * @author     Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @class      LucidModel
 */
class LucidModel extends Model {

  /**
   * Disable timestamps for this model.
   *
   * @return     {Array}
   */
  static get dates () {
    return []
  }

  /**
   * Translatable attributes.
   *
   * @return     {Array}
   */
  static get translatedAttributes () {
    return []
  }

  /**
   * Searchable attributes.
   *
   * @return     {Array}
   */
  static get searchableAttributes () {
    return []
  }

  /**
   * The number of minutes to cache query results.
   *
   * @return     {Boolean}
   */
  static get ttl () {
    return false
  }

  /**
   * The attributes that are not mass assignable.
   * Let upper models to handle this themselves.
   *
   * @return     {Array}
   */
  get guarded () {
    return []
  }

  /**
   * The title key.
   *
   * @return     {String}
   */
  static get titleKey () {
    return 'id'
  }

  /**
   * Observable model events.
   *
   * @return     {Array}
   */
  static get observables () {
    return [
      'updatingMultiple',
      'updatedMultiple',
      'deletingMultiple',
      'deletedMultiple',
    ]
  }

  /**
   * The cascading delete-able relations.
   *
   * @return     {Array}
   */
  static get cascades () {
    return []
  }

  /**
   * Runtime cache.
   *
   * @return     {Array}
   */
  static get cache () {
    return []
  }

  /**
   * Gets the identifier.
   *
   * @return     {Number}  The identifier.
   */
  getId () {
    return this.id
  }

  /**
   * Cache a value in the model's cache collection.
   *
   * @param      {String}  key    The key
   * @param      {Number}  ttl    The ttl
   * @param      {String}  value  The value
   * @return     {Mixed}
   */
  // cache (key, ttl, value) {
  //   new CacheCollection()
  //     .make([key])
  //     .setKey(this.getCacheCollectionKey())
  //     .index();

  //   return app('cache').remember(
  //     key,
  //     ttl || this.getTtl(),
  //     value
  //   );
  // }

  /**
   * Fire a model event.
   *
   * @param      {Object}  event   The event
   * @return     {Mixed}
   */
  // fireEvent (event) {
  //   if (!this.$dispatcher) {
  //     return true
  //   }

  //   event = `lucid.${event}:`
  //   return this.fireModelEvent(event);
  // }

  /**
   * Return the entry presenter.
   *
   * This is against standards but required
   * by the presentable interface.
   *
   * @return     {LucidPresenter}
   */
  getPresenter () {
    const presenterName = this.constructor.name.slice(0, -5) + 'Presenter'

    let presenter = this._getClassInstance(presenterName)

    if (presenter instanceof LucidPresenter) {
      return presenter
    }

    return Reflect.construct(
      use(this.constructor.$namespace.replace(
        this.constructor.name,
        presenterName
      )),
      [this]
    )
  }

  /**
   * Return the entry collection.
   *
   * This is against standards but required
   * by the presentable interface.
   *
   * @return     {LucidCollection}
   */
  newCollection () {
    const collectionName = this.constructor.name.slice(0, -5) + 'Collection'

    let collection = this._getClassInstance(collectionName)

    if (collection instanceof LucidCollection) {
      return collection
    }

    return Reflect.construct(
      use(this.constructor.$namespace.replace(
        this.constructor.name,
        collectionName
      )),
      [this]
    )
  }

  /**
   * Return the translatable flag.
   *
   * @return     {Boolean}
   */
  isTranslatable () {
    return Boolean(this.translationModel)
  }

  /**
   * Set the translatable flag.
   *
   * @param      {Boolean}
   * @return $this
   */
  setTranslatable (translatable) {
    this.translatable = translatable
    return this
  }

  /**
   * Set the ttl.
   *
   * @param      {Number}
   */
  setTtl (ttl) {
    this.ttl = ttl
    return this
  }

  /**
   * Get the ttl.
   *
   * @return     {Number}
   */
  getTtl () {
    return this.ttl
  }

  /**
   * Get cache collection key.
   *
   * @return     {String}
   */
  getCacheCollectionKey () {
    return this.$namespace
  }

  /**
   * Get the model title.
   *
   * @return     {Mixed}
   */
  getTitle () {
    return this[this.getTitleName()]
  }

  /**
   * Get the title key.
   *
   * @return     {String}
   */
  getTitleName () {
    return this.titleName || 'id'
  }

  /**
   * Return if a row is deletable or not.
   *
   * @return     {Boolean}
   */
  isDeletable () {
    return true
  }

  /**
   * Return if the model is restorable or not.
   *
   * @return     {Boolean}
   */
  isRestorable () {
    return true
  }

  /**
   * Return whether the model is being force deleted or not.
   *
   * @return     {Boolean}  True if force deleting, False otherwise.
   */
  isForceDeleting () {
    return Reflect.has(this, 'forceDeleting') && this.forceDeleting === true
  }

  /**
   * Gets the presenter instance.
   *
   * @private
   *
   * @param      {String}               name    The name
   * @return     {Presenter|undefined}  The presenter instance.
   */
  _getClassInstance (name) {
    let presenter

    try {
      presenter = require('./' + name)
    } catch (error) {
      return
    }

    return Reflect.construct(presenter, [this])
  }

  /**
   * Get the namespace of the class
   *
   * @return     {String}
   */
  static get $namespace () {
    const { _moduleAliases } = require('@root/package.json')
    const folder = _moduleAliases['@streams']

    return require('@adonisjs/fold').resolver
      .forDir(__dirname.replace(folder, '@streams'))
      .translate(this.name).replace('@streams/', '')
  }

}

module.exports = LucidModel
