'use strict'

const Collection = use('Streams/Platform/Support/Collection')

/**
 * Class for the base Lucid collection.
 *
 * @author     Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @class      LucidCollection
 */
class LucidCollection extends Collection {

}

module.exports = LucidCollection
