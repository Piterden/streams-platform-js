'use string'

const { camelCase, snakeCase } = require('lodash')

/**
 * Class for the base Lucid repository.
 *
 * @property   {LucidModel|EntryModel}  _model  The model
 *
 * @author     Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @class      LucidRepository
 */
class LucidRepository {

  /**
   * Constructs the object.
   *
   * @param      {LucidModel|EntryModel}  model   The model
   * @return     {LucidRepository|Proxy}
   */
  constructor (model) {
    this._model = model
    return new Proxy(this, {
      get (that, key) {
        if (Reflect.has(that, key)) {
          return that[key]
        }
        // TODO
        return function () {
          return snakeCase(key)
        }
      }
    })
  }

  /**
   * Return all records.
   *
   * @return     {LucidCollection}
   */
  all () {
    return this._model.all()
  }

  /**
   * Find a record by it's ID.
   *
   * @param      {Number}         id      The identifier
   * @return     {LucidModel}
   */
  find (id) {
    return this._model.find(id)
  }

  /**
   * Find a record by it's column value.
   *
   * @param      {String}      column  The column
   * @param      {Mixed}       value   The value
   * @return     {LucidModel}
   */
  findBy (column, value) {
    return this._model.where(column, value).first()
  }

  /**
   * Find all records by IDs.
   *
   * @param      {Array}            ids     The identifiers
   * @return     {LucidCollection}
   */
  findAll (ids) {
    return this._model.whereIn('id', ids).get()
  }

  /**
   * Find a trashed record by it's ID.
   *
   * @param      {Number}      id      The identifier
   * @return     {LucidModel}
   */
  findTrashed (id) {
    return this._model
      .withTrashed()
      .orderBy('id', 'ASC')
      .where('id', id)
      .first()
  }

  /**
   * Create a new record.
   *
   * @param      {Object}       attributes  The attributes
   * @return     {LucidModel}
   */
  create (attributes = {}) {
    return this._model.create(attributes)
  }

  /**
   * Return a new query builder.
   *
   * @return     {Builder}
   */
  newQuery () {
    return this._model.newQuery()
  }

  /**
   * Return a new instance.
   *
   * @param      {Object}      attributes  The attributes
   * @return     {LucidModel}
   */
  newInstance (attributes = {}) {
    return this._model.newInstance(attributes)
  }

  /**
   * Count all records.
   *
   * @return     {Number}
   */
  count () {
    return this._model.count()
  }

  /**
   * Return a paginated collection.
   *
   * @param      {Object}     parameters  The parameters
   * @return     {Paginator}
   */
  paginate (parameters = {}) {
    const query = this._model.newQuery()
    const Config = use('Adonis/Src/Config')

    parameters.per_page = parameters.per_page ||
      Config.get('streams::system.per_page', 15)

    if (parameters.scope) {
      Reflect.apply(
        query[camelCase(parameters.scope)],
        undefined,
        parameters.scope_arguments
      )
    }

    parameters.forEach((method, args) => {

      if (['update', 'delete', 'paginator', 'per_page', 'scope']
        .contains(method)) {
        return
      }

      method = camelCase(method)

      if (Array.isArray(args)) {
        Reflect.apply(query[method], undefined, args)
        return
      }

      Reflect.apply(query[method], undefined, [args])
    })

    return parameters.paginator === 'simple'
      ? query.simplePaginate(parameters.per_page)
      : query.paginate(parameters.per_page)
  }

  /**
   * Save a record.
   *
   * @param      {LucidModel}  entry   The entry
   * @return     {Boolean}
   */
  save (entry) {
    return entry.save()
  }

  /**
   * Update multiple records.
   *
   * @param      {Object}   attributes  The attributes
   * @return     {Boolean}
   */
  update (attributes = {}) {
    return this._model.update(attributes)
  }

  /**
   * Delete a record.
   *
   * @param      {LucidModel}  entry   The entry
   * @return     {Boolean}
   */
  delete (entry) {
    return entry.delete()
  }

  /**
   * Force delete a record.
   *
   * @param      {LucidModel}  entry   The entry
   * @return     {Boolean}
   */
  forceDelete (entry) {
    entry.forceDelete()
    return !entry.exists
  }

  /**
   * Restore a trashed record.
   *
   * @param      {LucidModel}  entry   The entry
   * @return     {Boolean}
   */
  restore (entry) {
    return entry.restore()
  }

  /**
   * Truncate the entries.
   *
   * @return     {this}
   */
  truncate () {
    this._truncate(this._model)

    if (this._model.isTranslatable()) {
      this._truncate(this._model.getTranslationModel())
    }

    return this
  }

  /**
   * Truncate entries of the given model
   *
   * @private
   *
   * @param      {LucidModel}  model   The model
   */
  _truncate (model) {
    const self = this

    model.flushCache()

    model.all().forEach((entry) => {
      self.delete(entry)
    })

    model.truncate()
  }

  /**
   * Cache a value in the model's cache collection.
   *
   * @param      {String}  key     The key
   * @param      {Number}  ttl     The ttl
   * @param      {Mixed}   value   The value
   * @return     {Mixed}
   */
  cache (key, ttl, value) {
    return this._model.cache(key, ttl, value)
  }

  /**
   * Flush the cache.
   *
   * @return $this
   */
  flushCache () {
    this._model.flushCache()

    return this
  }

  /**
   * Guard the model.
   *
   * @return $this
   * @return     {this}
   */
  guard () {
    this._model.reguard()

    return this
  }

  /**
   * Unguard the model.
   *
   * @return     {this}
   */
  unguard () {
    this._model.unguard()

    return this
  }

  /**
   * Get the model.
   *
   * @return     {LucidModel}
   */
  getModel () {
    return this._model
  }

  /**
   * Set the model.
   *
   * @param      {LucidModel}  model   The model
   * @return     {this}
   */
  setModel (model) {
    this._model = model

    return this
  }

}

module.exports = LucidRepository
