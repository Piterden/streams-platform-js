'use strict'

const Presenter = use('Streams/Platform/Support/Presenter')

/**
 * Class LucidPresenter
 *
 * @property   {Mixed}  _object  The presented object
 *
 * @author     Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @class      LucidPresenter
 */
class LucidPresenter extends Presenter {}

module.exports = LucidPresenter
