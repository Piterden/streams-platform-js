'use strict'

const GenericCollection = require('collections/generic-collection')

/**
 * Class for the base collection.
 *
 * @author     Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @class      Collection
 */
class Collection extends GenericCollection {

}

module.exports = Collection
