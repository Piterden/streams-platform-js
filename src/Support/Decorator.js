'use strict'

const Presenter = use('Streams/Platform/Support/Presenter')

/**
 * Class for decorator.
 *
 * @class      Decorator
 */
class Decorator {

  /**
   * Decorate the value
   *
   * @param      {Mixed}  value   The value
   * @return     {Mixed}
   */
  decorate (value) {
    const self = this

    if (Reflect.has(value, 'getPresenter')) {
      return value.getPresenter()
    }

    if (Array.isArray(value)) {
      return value.map((v) => self.decorate(v))
    }

    return value
  }

  /**
   * Undecorate the value
   *
   * @param      {Mixed}  value   The value
   * @return     {Mixed}
   */
  undecorate (value) {
    const self = this

    if (value instanceof Presenter) {
      return value.getObject()
    }

    if (Array.isArray(value)) {
      return value.map((v) => self.undecorate(v))
    }

    return value
  }

}

module.exports = Decorator
