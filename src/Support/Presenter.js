'use strict'

const Decorator = use('Streams/Platform/Support/Decorator')

/**
 * Front-end that give access to a real presentable.
 *
 * @class      {PresentableProxy}
 */
class PresentableProxy {

  /**
   * Constructs the object.
   *
   * @param      {Mixed}  object  The object
   * @return     {Proxy}
   */
  constructor (object) {
    this._object = object
    return new Proxy(this, {
      get (target, property, receiver) {
        return property
      }
    })
  }

}

/**
 * Class for presenter.
 *
 * @class      {Presenter}
 */
class Presenter extends PresentableProxy {

  /**
   * Decorator instance
   *
   * @return     {Decorator}
   */
  static get _decorator () {
    if (!Presenter._decorator) {
      Presenter._decorator = new Decorator()
    }

    return Presenter._decorator
  }

  /**
   * This is so you can extend the decorator and inject it into
   * the presenter at the class level so the new decorator will be
   * used for nested presenters. Method name should be "setDecorator"
   * however like above I want to make conflicts less likely.
   *
   * @param      {Decorator}
   */
  static setExtendedDecorator (decorator) {
    if (!(decorator instanceof Decorator)) {
      return
    }

    Presenter._decorator = decorator
  }

  /**
   * Gets the object.
   *
   * @return     {Mixed}  The object.
   */
  getObject () {
    return this._object
  }

  async dispatch (command) {
    const result = await command.handle(...command.includes)
    return result
  }

}

module.exports = Presenter
