'use strict'

const Schema = use('Schema')

class CreateAssignmentsTableTableSchema extends Schema {

  /**
   * Up the migration
   */
  up () {
    this.createIfNotExists('streams_assignments', (table) => {
      table.increments('id')
      table.integer('sort_order')
      table.integer('stream_id')
      table.integer('field_id')
      table.text('config')
      table.boolean('unique').defaultTo(0)
      table.boolean('required').defaultTo(0)
      table.boolean('translatable').defaultTo(0)

      table.unique(['stream_id', 'field_id'], 'unique_assignments')
    })

    this.createIfNotExists('streams_assignments_translations', (table) => {
      table.increments('id')
      table.integer('assignment_id')
      table.string('locale').index()
      table.string('label').nullable()
      table.string('warning').nullable()
      table.string('placeholder').nullable()
      table.text('instructions').nullable()
    })
  }

  /**
   * Down the migration
   */
  down () {
    this.dropIfExists('streams_assignments')
    this.dropIfExists('streams_assignments_translations')
  }

}

module.exports = CreateAssignmentsTableTableSchema
