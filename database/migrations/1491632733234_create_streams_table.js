'use strict'

const Schema = use('Schema')

class CreateStreamsTableTableSchema extends Schema {

  /**
   * Up the migration
   */
  up () {
    this.createIfNotExists('streams_streams', (table) => {
      table.increments('id')
      table.string('namespace', 150)
      table.string('slug', 150)
      table.string('prefix').nullable()
      table.string('title_column').defaultTo('id')
      table.string('order_by').defaultTo('id')
      table.boolean('locked').defaultTo(0)
      table.boolean('hidden').defaultTo(0)
      table.boolean('sortable').defaultTo(0)
      table.boolean('searchable').defaultTo(0)
      table.boolean('trashable').defaultTo(0)
      table.boolean('translatable').defaultTo(0)
      table.text('config')

      table.unique(['namespace', 'slug'], 'unique_streams')
    })

    this.createIfNotExists('streams_streams_translations', (table) => {
      table.increments('id')
      table.integer('stream_id')
      table.string('locale').index()
      table.string('name').nullable()
      table.string('description').nullable()
    })
  }

  /**
   * Down the migration
   */
  down () {
    this.dropIfExists('streams_streams')
    this.dropIfExists('streams_streams_translations')
  }

}

module.exports = CreateStreamsTableTableSchema
