'use strict'

const Schema = use('Schema')

class CreateNotificationsTableTableSchema extends Schema {

  /**
   * Up the migration
   */
  up () {
    this.createIfNotExists('notifications', (table) => {
      table.uuid('id').primary()
      table.string('type')
      table.integer('notifiable_id')
      table.string('notifiable_type')
      table.text('data')
      table.timestamp('read_at').nullable()
      table.timestamps()
    })
  }

  /**
   * Down the migration
   */
  down () {
    this.dropIfExists('notifications')
  }

}

module.exports = CreateNotificationsTableTableSchema
