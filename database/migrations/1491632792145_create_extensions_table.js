'use strict'

const Schema = use('Schema')

class CreateExtensionsTableTableSchema extends Schema {

  /**
   * Up the migration
   */
  up () {
    this.createIfNotExists('addons_extensions', (table) => {
      table.increments('id')
      table.string('namespace').index('unique_extensions').unique()
      table.boolean('installed').defaultTo(0)
      table.boolean('enabled').defaultTo(0)
    })
  }

  /**
   * Down the migration
   */
  down () {
    this.dropIfExists('addons_extensions')
  }

}

module.exports = CreateExtensionsTableTableSchema
