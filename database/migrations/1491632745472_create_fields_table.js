'use strict'

const Schema = use('Schema')

class CreateFieldsTableTableSchema extends Schema {

  /**
   * Up the migration
   */
  up () {
    this.createIfNotExists('streams_fields', (table) => {
      table.increments('id')
      table.string('namespace', 150)
      table.string('slug', 150)
      table.string('type')
      table.text('config')
      table.boolean('locked').defaultTo(0)

      table.unique(['namespace', 'slug'], 'unique_fields')
    })

    this.createIfNotExists('streams_fields_translations', (table) => {
      table.increments('id')
      table.integer('field_id')
      table.string('locale').index()
      table.string('name').nullable()
      table.string('placeholder').nullable()
      table.string('warning').nullable()
      table.text('instructions').nullable()
    })
  }

  /**
   * Down the migration
   */
  down () {
    this.dropIfExists('streams_fields')
    this.dropIfExists('streams_fields_translations')
  }

}

module.exports = CreateFieldsTableTableSchema
