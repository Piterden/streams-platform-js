'use strict'

const Schema = use('Schema')

class CreateJobsTableTableSchema extends Schema {

  /**
   * Up the migration
   */
  up () {
    this.createIfNotExists('jobs', (table) => {
      table.bigIncrements('id')
      table.string('queue')
      table.text('payload', 'longtext')
      table.integer('attempts').unsigned()
      table.integer('reserved_at').unsigned().nullable()
      table.integer('available_at').unsigned()
      table.integer('created_at').unsigned()

      table.unique(['queue', 'reserved_at'])
    })
  }

  /**
   * Down the migration
   */
  down () {
    this.dropIfExists('jobs')
  }

}

module.exports = CreateJobsTableTableSchema
