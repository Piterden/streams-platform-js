'use strict'

const Schema = use('Schema')

/**
 * Class for create applications table table schema.
 *
 * @class      CreateApplicationsTableTableSchema
 */
class CreateApplicationsTableTableSchema extends Schema {

  /**
   * Up the migration
   */
  up () {
    this.createIfNotExists('applications', (table) => {
      table.increments('id')
      table.string('name')
      table.string('reference').index('unique_application_references').unique()
      table.string('domain').index('unique_application_domains').unique()
      table.boolean('enabled')
    })
  }

  /**
   * Down the migration
   */
  down () {
    this.dropIfExists('applications')
  }

}

module.exports = CreateApplicationsTableTableSchema
