'use strict'

const Schema = use('Schema')

class CreateApplicationsDomainsTableTableSchema extends Schema {

  /**
   * Up the migration
   */
  up () {
    this.createIfNotExists('applications_domains', (table) => {
      table.increments('id')
      table.integer('application_id')
      table.string('domain').index('unique_application_aliases').unique()
      table.string('locale')
    })
  }

  /**
   * Down the migration
   */
  down () {
    this.dropIfExists('applications_domains')
  }

}

module.exports = CreateApplicationsDomainsTableTableSchema
