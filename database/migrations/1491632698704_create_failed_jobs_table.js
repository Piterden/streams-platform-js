'use strict'

const Schema = use('Schema')

class CreateFailedJobsTableTableSchema extends Schema {

  /**
   * Up the migration
   */
  up () {
    this.createIfNotExists('failed_jobs', (table) => {
      table.increments('id')
      table.text('connection')
      table.text('queue')
      table.text('payload', 'longtext')
      table.text('exception', 'longtext')
      table.timestamp('failed_at')
    })
  }

  /**
   * Down the migration
   */
  down () {
    this.dropIfExists('failed_jobs')
  }

}

module.exports = CreateFailedJobsTableTableSchema
