'use strict'

const Schema = use('Schema')

class CreateModulesTableTableSchema extends Schema {

  /**
   * Up the migration
   */
  up () {
    this.createIfNotExists('addons_modules', (table) => {
      table.increments('id')
      table.string('namespace').index('unique_modules').unique()
      table.boolean('installed').defaultTo(0)
      table.boolean('enabled').defaultTo(0)
    })
  }

  /**
   * Down the migration
   */
  down () {
    this.dropIfExists('addons_modules')
  }

}

module.exports = CreateModulesTableTableSchema
